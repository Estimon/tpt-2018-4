const double = require("./double");

test("double of 1 is 2", () => {
	expect(double(1)).toBe(2);
});

test("double of 3 is 6", () => {
	expect(double(3)).toBe(6);
});

test("double gets error with string input", () => {
	expect(() => {
		double("string");
	}).toThrow();   
});

test("double of 4 is 8", () => {
	expect(double(4)).toBe(8);
});