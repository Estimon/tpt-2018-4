module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "node": true,
        "jest": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 6
    },
    "rules": {
        "indent": [
            "error",
             2
            
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "double"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-console":[
            "off"
        ],
        "no-undef":[
            "off"
        ],
        "no-redeclare":[
            "off"
        ],
        "no-useless-escape":[
            "off"
        ],
        "no-unused-vars":[
            "off"
        ]
    }
};